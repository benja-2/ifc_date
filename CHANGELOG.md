## [Unreleased]

## [0.2.0] - 2022-07-02

* Add `to_s` and `inspect` method to return ISO8601 style representation
* Attach the starday to the week it attaches instead of using zero.
  So  '2022-12-31'.to\_date.ifc.week will be 52 instead of 0
  and '2020-06-17'.to\_date.ifc.week will be 24 instead of 0
* more consistent implementation using the cleaned divmod structure

### [0.2.1] - 2022-07-02

* Code refactor to have better grouping of concerns

## [0.1.0] - 2022-06-08

- Initial release



