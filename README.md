# IfcDate

International Fixed Calendar system. Other name references are Cotsworth and Eastman.
This implementation is derived from the specs on:
  (https://en.wikipedia.org/wiki/International_Fixed_Calendar)
And with code inspiraction from:
  (https://github.com/msampathkumar/ifcalendar)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ifc_date'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install ifc_date

## Usage

To create an IfcDate object you specify a day of the year and a year:
```ruby
  ifc_date = IfcDate.new(183, 2021)
```
or from a standard `Date` object
```ruby
  Date.today.ifc
```

### Concepts
It is really important to know how the indices of the different time units are:

#### starday?
```ruby
  ifc_date.starday?
```
This is a concept introduced in this project. Since there is already a Moonday and a Sunday,
The intermittant days, Year day and Leap day can be called Starday, it works in the code.

#### day_of_year, yday
```ruby
  ifc_date.day_of_year # alias ifc_date.yday
```
This is the day of the year, ranges from 1-365 for non leap year, 1-366 for leap year.

#### day_of_week, wday
```ruby
  ifc_date.day_of_week # alias ifc_date.wday
```
This is the day of the week. Ranges from 0-6, Indicating Sunday-Saturday.
There is no days starting on Monday concept.
When it is Starday a 7 will be returned.
This will work in the `ifc_date.day_name` and `ifc_date.abbr_day_name` methods

#### week_of_year, week
```ruby
  ifc_date.week_of_year # alias ifc_date.week
```
This returns the number of the week of the year. Ranges from 1-52. Note that the legacy date system
can have week numbers 53 both in December and Januari, this will be the past.

At the moment on a Starday the week number will return 0. Personally I might prefer to add the
Starday week\_of\_year number to the week number that preceeds it. But I still have to
investigate the validity of this one based on the specs.

#### day_of_month, mday
```ruby
  ifc_date.day_of_month # alias ifc_date.mday
```
This returns the index of the day in the month. Ranges 1-28

#### month_of_year, month
```ruby
  ifc_date.month_of_year # alias ifc_date.month
```
This returns the index of the month in the year. Ranges 1-13.
The Starday days are added to the month number that preceeds those days as day number 29.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/ifc_date.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
