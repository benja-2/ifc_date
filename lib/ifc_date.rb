# frozen_string_literal: true

require 'date'
require 'active_support/concern'
require_relative 'ifc_date/version'
require_relative 'ifc_date/ext/to_ifc'
require_relative 'ifc_date/memoization'
require_relative 'ifc_date/localized_formatting'

class IfcDate
  class Error < StandardError; end
  class MissingTranslationData < StandardError; end
  include Memoization
  include LocalizedFormatting

  attr_reader :day_of_year, :year
  alias yday day_of_year

  DAYNAMES = Date::DAYNAMES + ['Starday']
  ABBR_DAYNAMES = Date::ABBR_DAYNAMES + ['Sta']
  MONTHNAMES = Date::MONTHNAMES.dup.insert(7, 'Sol')
  ABBR_MONTHNAMES = Date::ABBR_MONTHNAMES.dup.insert(7, 'Sol')

  # Make leap year inquiry available on class level to avoid dummy day and month requirement
  def self.is_leap_year?(year)
    return false unless (year % 4).zero?
    return true unless (year % 100).zero?
    (year % 400).zero?
  end

  def initialize(day_of_year = nil, year = nil)
    unless year
      date = Date.today
      year = date.year
      day_of_year ||= date.yday
    end

    day_of_year ||= date.yday

    @day_of_year = day_of_year
    @year        = year
  end

  memoize def leap_year?
    self.class.is_leap_year?(year)
  end

  # This method caches the divmod result of the:
  # zero based (working number starts with 1) day of the year
  # where Leap day is removed. By applying the divmod on the
  # 'cleaner' representation of the day of the year, other parts
  # of the code become easier to reason about.
  memoize def day_of_year_divmod_seven
    if leap_year? && day_of_year > 168
      (day_of_year - 2).divmod(7)
    else
      (day_of_year - 1).divmod(7)
    end
  end

  # This method caches the divmod result of the:
  # zero based (working number starts with 1) day of the year
  # where Leap day is removed. By applying the divmod on the
  # 'cleaner' representation of the day of the year, other parts
  # of the code become easier to reason about.
  memoize def day_of_year_divmod_twenty_eight
    if leap_year? && day_of_year > 168
      (day_of_year - 2).divmod(28)
    else
      (day_of_year - 1).divmod(28)
    end
  end

  memoize def starday?
    if leap_year?
      return true if [169, 366].include?(day_of_year)
    else
      return true if day_of_year == 365
    end
    false
  end

  memoize def week_of_year
    # The min operation if for the year day, which would be 53, but is appended to week 52
    [day_of_year_divmod_seven[0] + 1, 52].min
  end
  alias week week_of_year

  memoize def day_of_week
    return 7 if starday?
    day_of_year_divmod_seven[1]
  end
  alias wday day_of_week

  def day_of_month
    return 29 if starday?
    day_of_year_divmod_twenty_eight[1] + 1
  end
  alias mday day_of_month

  # Return the month in index of the year.
  # NOTE January will be 1, so different indexing thant the wday
  def month_of_year
    # The min operation if for the year day, which would be 14, but is appended to month 13
    [day_of_year_divmod_twenty_eight[0] + 1, 13].min
  end
  alias month month_of_year
  alias mon month_of_year

  def day_name
    DAYNAMES[day_of_week]
  end

  def abbr_day_name
    ABBR_DAYNAMES[day_of_week]
  end

  def month_name
    MONTHNAMES[month_of_year]
  end

  def abbr_month_name
    ABBR_MONTHNAMES[month_of_year]
  end

  def inspect
    sprintf("%04d-%02d-%02d", year, month_of_year, day_of_month)
  end
  alias to_s inspect
  alias iso8601 to_s

end
