# frozen_string_literal: true

class IfcDate
  module LocalizedFormatting
    extend ActiveSupport::Concern
    IFC_LOCALIZATIONS = {
      en: {starday: 'Starday',    abbr_starday: 'Sta', sol: 'Sol', abbr_sol: 'Sol'},
      es: {starday: 'Estrellés',  abbr_starday: 'Est', sol: 'sol', abbr_sol: 'sol'},
      nl: {starday: 'Sterdag',    abbr_starday: 'Ste', sol: 'Sol', abbr_sol: 'Sol'},
      de: {starday: 'Sternentag', abbr_starday: 'Ste', sol: 'Sol', abbr_sol: 'Sol'},
    }

    # This method is a tricky one and for sure incomplete.
    # Based on: https://github.com/ruby-i18n/i18n/blob/master/lib/i18n/backend/base.rb method
    #   translate_localization_format
    def strftime(format = nil)
      unless format and format['%']
        # :short or :long
        format = I18n.translate!("date.formats.#{format || :default}")
      end
      localized_config = IFC_LOCALIZATIONS[I18n.locale]
      result = format.to_s.gsub(/%(|\^)[aAbBpPyYCn]/) do |match|
        case match
        when '%a'  then (I18n.t(:'date.abbr_day_names', default: Date::ABBR_DAYNAMES) + [IFC_LOCALIZATIONS[I18n.locale][:abbr_starday]])[day_of_week]
        when '%^a' then (I18n.t(:'date.abbr_day_names', default: Date::ABBR_DAYNAMES) + [IFC_LOCALIZATIONS[I18n.locale][:abbr_starday]])[day_of_week].upcase
        when '%A'  then (I18n.t(:'date.day_names', default: Date::DAYNAMES) + [IFC_LOCALIZATIONS[I18n.locale][:starday]])[day_of_week]
        when '%^A' then (I18n.t(:'date.day_names', default: Date::DAYNAMES) + [IFC_LOCALIZATIONS[I18n.locale][:starday]])[day_of_week].upcase
        when '%b'  then I18n.t(:'date.abbr_month_names', default: Date::ABBR_MONTHNAMES).dup.insert(7, localized_config[:abbr_sol])[month_of_year]
        when '%^b' then I18n.t(:'date.abbr_month_names', default: Date::ABBR_MONTHNAMES).dup.insert(7, localized_config[:abbr_sol])[month_of_year].upcase
        when '%B'  then I18n.t(:'date.month_names', default: Date::MONTHNAMES).dup.insert(7, localized_config[:sol])[month_of_year]
        when '%^B' then I18n.t(:'date.month_names', default: Date::MONTHNAMES).dup.insert(7, localized_config[:sol])[month_of_year].upcase
        when '%Y'  then '%04d' % year
        when '%y'  then (year % 100).to_s
        when '%C'  then (year / 100).to_s
        when '%n'  then $/
        end
      end
      result.gsub!(/%(|\-)[dme]/) do |match|
        case match
        when '%e', '%-d' then day_of_month.to_s
        when '%d'  then '%02d' % day_of_month
        end
      end
      result
    end

  end
end
