# frozen_string_literal: true

class IfcDate
  module Memoization
    extend ActiveSupport::Concern

    # In case a crucial element of the object itself changes the memoization should be cleared.
    # Note that this is not expected since the current way of reasoning assumes new object creation.
    # Maybe something of the future could be:
    #   date = Date.today.ifc
    #   date.move_forward 3.days
    # But this is for later consideration
    def clear_memoization_results
      @_memoized_methods.clear
    end

    class_methods do
      # Do not execute computational expensive operations more than once.
      # NOTE: only use for explicitly non nil return methods, false is fine
      def memoize(method_name)
        unmemoized_name = "_unmemoized_#{method_name}"
        cache_key = method_name.end_with?('?') ? "is_#{method_name[0..-2]}" : method_name.to_s
        alias_method unmemoized_name, method_name
        define_method method_name do |*args, &block|
          @_memoized_methods ||= {}
          return @_memoized_methods[cache_key] if @_memoized_methods.has_key?(cache_key)
          evaluated_result = send(unmemoized_name, *args, &block)
          @_memoized_methods[cache_key] = evaluated_result
        end
      end
    end
  end
end
