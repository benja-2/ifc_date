
RSpec.describe Date do
  it "converts to proper ifc date" do
    subject = Date.parse('2021-08-24').ifc
    expect( subject ).to be_a IfcDate
    expect( subject.yday ).to eq 236
    expect( subject.year ).to eq 2021
  end
end
