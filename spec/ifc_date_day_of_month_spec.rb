# frozen_string_literal: true

RSpec.describe IfcDate do

  it "returns 28 for leap year day 365" do
    subject = IfcDate.new 365, 2000
    expect(subject.mday).to eq 28
  end

  describe "mday, day_of_month in non leap year" do
    mapping = {
      '2021-12-30' => 28,
      '2021-12-31' => 29,
      '2022-01-01' =>  1,
      '2022-01-02' =>  2,
      '2022-01-03' =>  3,
      '2022-01-04' =>  4,
      '2022-01-05' =>  5,
      '2022-01-06' =>  6,
      '2022-01-07' =>  7,
      '2022-01-08' =>  8,
      '2022-01-09' =>  9,
      '2022-06-16' => 27,
      '2022-06-17' => 28,
      '2022-06-18' =>  1,
      '2022-06-19' =>  2,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.day_of_month
      it "Maps #{date_str} to day_of_month of #{expected_value}" do
        expect(result).to eq(expected_value)
      end
    end
  end

  describe "mday, day_of_month in leap year" do
    mapping = {
      '1999-12-30' => 28,
      '1999-12-31' => 29,
      '2000-01-01' =>  1,
      '2000-01-02' =>  2,
      '2000-01-03' =>  3,
      '2000-01-04' =>  4,
      '2000-01-05' =>  5,
      '2000-01-06' =>  6,
      '2000-01-07' =>  7,
      '2000-01-08' =>  8,
      '2000-01-09' =>  9,
      '2000-06-16' => 28,
      '2000-06-17' => 29,
      '2000-06-18' =>  1,
      '2000-06-19' =>  2,
      '2000-12-30' => 28,
      '2000-12-31' => 29,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.day_of_month
      it "Maps #{date_str} to day_of_month of #{expected_value}" do
        expect(result).to eq(expected_value)
      end
    end
  end
end
