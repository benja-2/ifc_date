# frozen_string_literal: true

RSpec.describe IfcDate do

  describe "wday, day_of_week for non leap year" do
    mapping = {
      '2021-12-30' => 6,
      '2021-12-31' => 7,
      '2022-01-01' => 0,
      '2022-01-02' => 1,
      '2022-01-03' => 2,
      '2022-01-04' => 3,
      '2022-01-05' => 4,
      '2022-01-06' => 5,
      '2022-01-07' => 6,
      '2022-01-08' => 0,
      '2022-01-09' => 1,
      '2022-06-17' => 6,
      '2022-06-18' => 0,
      '2022-06-19' => 1,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.day_of_week
      it "Maps #{date_str} to day_of_week of #{expected_value}" do
        expect(result).to eq(expected_value) #, "Expected #{date_str} day_of_week to be #{expected_value} instead of #{result}"
      end
    end
  end

  describe "wday, day_of_week for leap year" do
    mapping = {
      '1999-12-30' => 6,
      '1999-12-31' => 7,
      '2000-01-01' => 0,
      '2000-01-02' => 1,
      '2000-01-03' => 2,
      '2000-01-04' => 3,
      '2000-01-05' => 4,
      '2000-01-06' => 5,
      '2000-01-07' => 6,
      '2000-01-08' => 0,
      '2000-01-09' => 1,
      '2000-06-16' => 6,
      '2000-06-17' => 7,
      '2000-06-18' => 0,
      '2000-06-19' => 1,
      '2000-12-30' => 6,
      '2000-12-31' => 7,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.day_of_week
      it "Maps #{date_str} to day_of_week of #{expected_value}" do
        expect(result).to eq(expected_value) #, "Expected #{date_str} day_of_week to be #{expected_value} instead of #{result}"
      end
    end
  end
end
