# frozen_string_literal: true

RSpec.describe IfcDate do
  before { travel_to Time.local(2021, 3, 9, 4, 2, 11) }
  after  { travel_back }

  it "works without any argument" do
    subject = IfcDate.new
    expect(subject.day_of_year).to eq 68
    expect(subject.yday).to eq 68
    expect(subject.year).to eq 2021
  end

  it "works with just a day of the year" do
    subject = IfcDate.new 69
    expect(subject.day_of_year).to eq 69
    expect(subject.yday).to eq 69
    expect(subject.year).to eq 2021
  end
end
