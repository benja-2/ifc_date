# frozen_string_literal: true

RSpec.describe IfcDate do
  describe "is_leap_year?" do
    LEAPS = [1600, 2000, 2400, 1996, 2004]
    NON_LEAPS = [1500, 1700, 1800, 1900, 2100, 1995, 1997, 1998, 1999, 2001, 2002, 2003, 2005]

    LEAPS.each do |i|
      it "indicates #{i} as a leap year" do
        expect(IfcDate.is_leap_year?(i)).to eq(true)
      end

      it "indicates #{i} as a leap year on the instance" do
        expect("#{i}-01-01".to_date.ifc.leap_year?).to eq(true)
      end
    end

    NON_LEAPS.each do |i|
      it "indicates #{i} NOT as a leap year" do
        expect(IfcDate.is_leap_year?(i)).to eq(false)
      end

      it "indicates #{i} NOT as a leap year on the instance" do
        expect("#{i}-01-01".to_date.ifc.leap_year?).to eq(false)
      end
    end
  end
end
