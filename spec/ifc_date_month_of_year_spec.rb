# frozen_string_literal: true

RSpec.describe IfcDate do

  describe "month_of_year, month for non leap year" do
    mapping = {
      '2021-12-30' => 13,
      '2021-12-31' => 13,
      '2022-01-01' =>  1,
      '2022-02-02' =>  2,
      '2022-04-03' =>  4,
      '2022-06-16' =>  6,
      '2022-06-17' =>  6,
      '2022-06-18' =>  7,
      '2022-06-19' =>  7,
      '2022-12-30' => 13,
      '2022-12-31' => 13,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.month_of_year
      it "Maps #{date_str} to month_of_year of #{expected_value}" do
        expect(result).to eq(expected_value)
      end
    end
  end

  describe "month_of_year, month for leap year" do
    mapping = {
      '1999-12-30' => 13,
      '1999-12-31' => 13,
      '2000-01-01' =>  1,
      '2000-02-02' =>  2,
      '2000-04-03' =>  4,
      '2000-06-16' =>  6,
      '2000-06-17' =>  6,
      '2000-06-18' =>  7,
      '2000-06-19' =>  7,
      '2000-12-30' => 13,
      '2000-12-31' => 13,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.month_of_year
      it "Maps #{date_str} to month_of_year of #{expected_value}" do
        expect(result).to eq(expected_value)
      end
    end
  end
end
