# frozen_string_literal: true

RSpec.describe IfcDate do
  describe 'Locale en' do
    before { I18n.locale = :en }
    it "has a proper long format in month Sol" do
      result = '2022-07-02'.to_date.ifc.strftime(:long)
      expect(result).to eq "Sol 15, 2022"
    end

    it "has a proper long format in month December" do
      result = '2022-12-31'.to_date.ifc.strftime(:long)
      expect(result).to eq "December 29, 2022"
    end
  end

  describe "Locale es" do
    before { I18n.locale = :es }
    it "has a proper long format in month Sol" do
      result = '2022-07-02'.to_date.ifc.strftime(:long)
      expect(result).to eq "15 de sol de 2022"
    end

    it "has a proper long format in month December" do
      result = '2022-12-31'.to_date.ifc.strftime(:long)
      expect(result).to eq "29 de diciembre de 2022"
    end
  end

  describe "Locale de" do
    before { I18n.locale = :de }
    it "has a proper long format in month Sol" do
      result = '2022-07-02'.to_date.ifc.strftime(:long)
      expect(result).to eq "15. Sol 2022"
    end

    it "has a proper long format in month December" do
      result = '2022-12-31'.to_date.ifc.strftime(:long)
      expect(result).to eq "29. Dezember 2022"
    end
  end
end
