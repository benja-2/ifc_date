# frozen_string_literal: true

RSpec.describe IfcDate do

  describe "week, week_of_year for non leap year" do
    mapping = {
      '2021-12-30' => 52,
      '2021-12-31' => 52,
      '2022-01-01' => 1,
      '2022-01-02' => 1,
      '2022-01-03' => 1,
      '2022-01-04' => 1,
      '2022-01-05' => 1,
      '2022-01-06' => 1,
      '2022-01-07' => 1,
      '2022-01-08' => 2,
      '2022-01-09' => 2,
      '2000-06-16' => 24,
      '2022-06-17' => 24, # day 168 of the year
      '2022-06-18' => 25,
      '2000-06-19' => 25,
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.week
      it "Maps #{date_str} to day_of_week of #{expected_value}" do
        expect(result).to eq(expected_value) #, "Expected #{date_str} day_of_week to be #{expected_value} instead of #{result}"
      end
    end
  end

  describe "week, week_of_year for leap year" do
    mapping = {
      '1999-12-30' => 52,
      '1999-12-31' => 52,
      '2000-01-01' => 1,
      '2000-01-02' => 1,
      '2000-01-03' => 1,
      '2000-01-04' => 1,
      '2000-01-05' => 1,
      '2000-01-06' => 1,
      '2000-01-07' => 1,
      '2000-01-08' => 2,
      '2000-01-09' => 2,
      '2000-06-16' => 24,
      '2000-06-17' => 24, # Leap day, Starday, day 169
      '2000-06-18' => 25,
      '2000-06-19' => 25,
      '2000-12-30' => 52,
      '2000-12-31' => 52, # Year day, Starday
    }
    mapping.each do |date_str, expected_value|
      ifc_date = date_str.to_date.ifc
      result = ifc_date.week_of_year
      it "Maps #{date_str} to day_of_week of #{expected_value}" do
        expect(result).to eq(expected_value) #, "Expected #{date_str} day_of_week to be #{expected_value} instead of #{result}"
      end
    end
  end
end
