# Invoke standalone as:
#   pry -e 'load "spec/rails_setup_base.rb"'

require 'rails/all'
#require 'active_support/all'
require 'ifc_date'
Bundler.require(:test)

I18n.config.available_locales = [:en, :es, :de]

def add_locales
  pattern = RailsI18n::Railtie.pattern_from I18n.available_locales
  RailsI18n::Railtie.add("rails/locale/#{pattern}.yml")
  I18n.locale = :es

end

add_locales
