# frozen_string_literal: true

require 'pry'
require "ifc_date"
require 'active_support/testing/time_helpers'
require 'active_support/core_ext/string/conversions' # String#to_date
require_relative 'rails_setup_base'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  config.include ActiveSupport::Testing::TimeHelpers
  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
